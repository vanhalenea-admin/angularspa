import { HeroesService, Heroe } from './../../../services/heroes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})
export class HeroesListComponent implements OnInit {

  heroes: Heroe[] = [];

  constructor(private _heroesService: HeroesService) { 
    
  }

    ngOnInit() {
      this.heroes = this._heroesService.getHeroes();
  }


}
