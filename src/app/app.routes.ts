import { HeroesComponent } from './components/heroes/heroes.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule } from '@angular/router';


const app_routes: Routes = [
  { path: 'home', component: HomeComponent } ,
  { path: 'heroes', component: HeroesComponent } ,
  { path: 'about', component: AboutComponent } ,
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const AppRouting: any = RouterModule.forRoot(app_routes);
